const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')

module.exports = {
  outputDir: 'home-web_'+(new Date().getMonth()+1)+'-'+new Date().getDate()+'_'+new Date().getHours()+'.'+new Date().getMinutes(),
  transpileDependencies: true,
  // 基础 URL, baseUrl 从 cli 3.3 开始弃用
  publicPath: process.env.NODE_ENV === 'production'
  ? './'
  : '',
  // 打包后的项目文件所在的目录名称，默认为: dist
  outputDir: 'Beelink_'+(new Date().getMonth()+1)+'-'+new Date().getDate()+'_'+new Date().getHours()+'.'+new Date().getMinutes(),

  // 是否生成生产环境（ -build- ）的 source map 
  productionSourceMap: false,

  // 生成的今天资源文件名称是否 hash , hash后可以解决缓存的问题
  filenameHashing: false,
  
  // 放置生成的静态资源 ( js、css、img、fonts , 相对于 outputDir , 默认为空 ) 
  assetsDir: "assets",
      
  // 指定生成的 index.html 的输出路径 (相对于 outputDir), 也可以是一个绝对路径, 默认值'index.html'
  indexPath: "index.html",
      
  // 是否使用包含运行时编译器的 Vue 构建版本。设置为 true 后你就可以在 Vue 组件中使用 template 选项了，但是这会让你的应用额外增加 10kb 左右。
  runtimeCompiler: false,
      
  // 是否在开发环境下通过 eslint-loader 在每次保存时 lint 代码。这个值会在 @vue/cli-plugin-eslint 被安装之后生效。
  lintOnSave: process.env.NODE_ENV !== "production",

  // 所有 webpack-dev-server 的选项都支持。
  // devServer: {
  //     host: "192.168.0.152", // 本地启动时的ip地址
  //     port: 8080,        // 端口号
  //     // https: true,
  //     open: true,        // 自动启动浏览器
  //     // TODO: 配置代理
  //     proxy: {
  //         '/': {    //将www.exaple.com印射为/index
  //         //target: 'http://192.168.0.186:9033',  // 接口域名
  //         // target:'https://scan.genesischain.io',
  //         target:'https://scan.genesischain.io',
  //         changeOrigin: true,  //是否跨域                 
  //         }
  //     },
  // }
  configureWebpack: {
    plugins: [
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  },
}
