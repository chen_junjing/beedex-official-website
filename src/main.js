import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import  i18n  from './language/index.js'
import 'element-plus/dist/index.css'
import "./assets/css/common.css";
import 'animate.css'
// 这块一定要加,否者会有部分动画无法执行
import 'animate.css/animate.compat.css'
import './assets/font/font.css';



createApp(App).use(store).use(router).use(i18n).mount('#app')
